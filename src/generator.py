## In this file there is work on password generation

import secrets
import string

def generate_passwd(option):
    chars = string.ascii_letters + string.digits + string.punctuation
    passwd = ''.join(secrets.choice(chars) for i in range(option))
    return passwd

