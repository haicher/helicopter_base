## In this file there is work on converting different values

class bcolor:
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

## Number system conversion
def block_numsys():
    print('\n────────────────────────────────────')
    print(bcolor.WARNING + 'Node.' + bcolor.ENDC + ' All operations are performed\n      with integers.\n\n      Decimal - dec\n      Binary - bin\n      Octal - oct\n      Hexadecimal - hex\n')

    print('      1. dec to bin\n      2. dec to oct\n      3. dec to hex')
    print('────────────────────────────────────\n')

    num = input('Enter a number for convert (default=1): ')

    if not num:
        num = '1'

    if num == '1':
        dc = int(input('dec: '))
        print('bin: ' + bin(dc))
    elif num == '2':
        dc = int(input('dec: '))
        print('oct: ' + oct(dc))
    elif num == '3':
        dc = int(input('dec: '))
        print('hex: ' + hex(dc))

## Number to Roman
def block_numto_rom():
    base_value = [1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000]
    i = 12

    num = int(input('Enter number: '))
    print('Roman: ', end='')

    while num > 0:
        quotient = num // base_value[i]
        num = num % base_value[i]

        while quotient > 0:
            if i == 0:
                print('I', end='')
            elif i == 1:
                print('IV', end='')
            elif i == 2:
                print('V', end='')
            elif i == 3:
                print('IX', end='')
            elif i == 4:
                print('X', end='')
            elif i == 5:
                print('XL', end='')
            elif i == 6:
                print('L', end='')
            elif i == 7:
                print('XC', end='')
            elif i == 8:
                print('C', end='')
            elif i == 9:
                print('CD', end='')
            elif i == 10:
                print('D', end='')
            elif i == 11:
                print('CM', end='')
            elif i == 12:
                print('M', end='')
            quotient -= 1
        i -= 1
    print('')

