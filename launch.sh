#!/bin/bash

## How comments are organized in the project
## '##' - to comment on plain text
## '#'  - to comment on code

if ! [ -d ".etc/"]
then
	mkdir .etc
fi

echo '1' > .etc/launch.txt
clear ; python main.py
echo '0' > .etc/launch.txt
