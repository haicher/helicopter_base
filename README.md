# Helicopter Base

Helicopter Base, or hcp_base is a console converter and password generator. On it, you can convert different number systems, and Roman numerals, or just generate a password of your choice.

---
#### Launch project
---
>```
>chmod +x ./launch.sh
>./launch.sh
>```

### Node
---
>If for some reason you don't have bash or it doesn't work, open the main.py file and comment out this part of the code:
>That's about line 104-110
>
>```python
>if os.path.exists('.etc/launch.txt'):
>        with open('.etc/launch.txt', 'r') as file:
>            content = file.read()
>        if content[0] != '1':
>            print(bcolor.FAIL + 'Error 2: To start the project enter this command: ./launch.sh' + bcolor.ENDC)
>            exit(1)
>    else:
>        print(bcolor.FAIL + 'Error 1: To start the project enter this command: ./launch.sh' + bcolor.ENDC)
>        exit(1)
>```

>At the moment in the item of number conversion operations with decimal numbers are realized, i.e. conversion from decimal number to binary, to octal and hexadecimal.
>
>And also I want to apologize for so many commit, when I first started working with git, I commited one file at a time, not in packages, thanks for your understanding.

![](./img/hcpbase.png)