import src.converter, src.generator

## Use for setting text color
class bcolor:
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

def generator():
    print('\n──────────────────────────────────────────')
    print(bcolor.WARNING + 'Node.' + bcolor.ENDC + ' Your password is very important. If\n      it is too simple it will be easy for\n      an attacker to hack you.')
    print('──────────────────────────────────────────\n')

    length = int(input('Enter the passwd size: '))
    print(src.generator.generate_passwd(length))

def converter():
    print('\n───────────────────────────────────────')
    print(bcolor.WARNING + 'Node.' + bcolor.ENDC + ' Here you can convert values to\n      different number systems, Roman\n      numerals.')
    print('───────────────────────────────────────\n')

    while 1:
        print('1. Number Systems\n2. Number to Roman\n3. Back\n')
        num = input('Enter a number (default=1): ')

        if not num:
            num = '1'

        if num == '1': 
            src.converter.block_numsys()
        elif num == '2':
            src.converter.block_numto_rom()
        elif num == '3': return
        else:
            print(bcolor.FAIL + '!. Invalid value: ' + num, bcolor.ENDC)
            exit(1)
        print('')



if __name__ == '__main__':
    if os.path.exists('.etc/launch.txt'):
        with open('.etc/launch.txt', 'r') as file:
            content = file.read()
        if content[0] != '1':
            print(bcolor.FAIL + 'Error 2: To start the project enter this command: ./launch.sh' + bcolor.ENDC)
            exit(1)
    else:
        print(bcolor.FAIL + 'Error 1: To start the project enter this command: ./launch.sh' + bcolor.ENDC)
        exit(1)

    print('''░█░█░█▀▀░█▀█░░░█▀▄░█▀█░█▀▀░█▀▀
░█▀█░█░░░█▀▀░░░█▀▄░█▀█░▀▀█░█▀▀
░▀░▀░▀▀▀░▀░░░░░▀▀░░▀░▀░▀▀▀░▀▀▀
''')

    while 1:
        print('Helicopter Base, or hcp_base is a consolen\nconverter and password generator. On it,\nyou can convert different number systems,\nand Roman numerals, or just generate a\npassword of your choice.')
        print('v. 2.0.1')

        print('\n1. Password generation\n2. Numbers converter\n3. Exit\n')
        num = input('Enter a number (default=1): ')

        if not num:
            num = '1'

        if num == '1': generator()
        elif num == '2': converter()
        elif num == '3': exit(0)
        else:
            print(bcolor.FAIL + '!. Invalid value: ' + num, bcolor.ENDC)
            exit(1)
        print('')
        print('──────────────────────────────────────────')
